r"""
.. module:: MTIpython.mech.principal
    :platform: Unix, Windows
    :synopsis: Mechanical engineering principal functions

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from MTIpython.mech.principal import axle, core

__all__ = ['core', 'axle', 'screwconveyor']
