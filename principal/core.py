r"""
.. module:: MTIpython.mech.principal.principal
    :platform: Unix, Windows
    :synopsis: Mechanical principal functions

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from enum import Enum, IntEnum

from numpy import load
from pkg_resources import resource_stream

from MTIpython.core.functions import MTIfunc, function_switch
from MTIpython.reference.bibliography import citation
from MTIpython.units.SI import isiterable

__all__ = ['power', 'stress', 'allowable_stress', 'torque', 'equivalent_torque', 'application_factor',
           'yield_safety_factor', 'equivalent_moment', 'LoadType', 'WorkingCharacteristic']

_source_roloff = r"""@book{roloff_2011,
                    address = {Den Haag},
                    edition = {4de},
                    title = {Roloff/ {Matek} {Machineonderdelen}: {Theoriebook}},
                    isbn = {978-90-395-2321-6},
                    language = {nl},
                    publisher = {SDU Uitgevers},
                    author = {Muhs, Dieter and Wittel, Herbert and Becker, Manfred and Jannasch, Dieter and Vossiek, 
                    Joachim},
                    year = {2011}
}
"""


class LoadType(Enum):
    r"""
    Load type
    """
    SHEAR = 1
    BENDING = 2
    TORSION = 3
    COMPRESSION = 4


class WorkingCharacteristic(IntEnum):
    r"""
    Working characteristic of a driving/driven machine
    """
    UNIFORM = 0
    LIGHT_SHOCKS = 1
    MODERATE_SHOCKS = 2
    HEAVY_SHOCKS = 3


@citation(_source_roloff)
def application_factor(driving_machine, driven_machine):
    r"""
    The application factor :math:`K_A` in :math:`[-]` according to Roloff/Matek table 3-5 or NEN-ISO 6336-1

    Args:
        driving_machine (:class:`.WorkingCharacteristic`): Working characteristic of the driving machine
        driven_machine (:class:`.WorkingCharacteristic`): Working characteristic of the driven machine

    Returns:
        float: Application factor :math:`K_A` in :math:`[-]`
    """
    with resource_stream('MTIpython', 'resources/mech/data/application_factor.npy') as f:
        app_fact = load(f)
    return float(app_fact[driven_machine, driving_machine])


class _ShearStress(MTIfunc):
    r"""
    Shear stress calculations either :math:`\tau_{sm}` or :math:`\tau_{zy}` both in :math:`[M^{1} L^{-1} t^{-2}]`

    .. math::
       \tau_{sm} = \frac{F_s}{A}

    **Traverse shear**

    .. math::
       \tau_{zy} = \frac{F_c(z) H_x(y)}{I_x w(y)} \text{, where } H_x(y) = \int_A y dA \text{ and } I_x = \int_A y^2 dA

    Args:
        F_c (:class:`~pint:.Quantity`): Compression force :math:`F_c` in :math:`[L^{1} M^{1} t^{-2}]`
        F_s (:class:`~pint:.Quantity`): Shear force :math:`F_s` in :math:`[L^{1} M^{1} t^{-2}]`
        A (:class:`~pint:.Quantity`): Surface :math:`A` in :math:`[L^{2}]`
        H_x (:class:`~pint:.Quantity`): Height :math:`H_x` in :math:`[L^{1}]`
        I_x (:class:`~pint:.Quantity`): Moment of inertia :math:`I_x` in :math:`[L^{4}]`
        w (:class:`~pint:.Quantity`): Width :math:`w` in :math:`[L^{1}]`

    Kwargs combinations:
        * F_s, A
        * F_c, H_x, I_x, w

    Returns:
        :class:`~pint:.Quantity`: Shear stress :math:`\tau_{sm}` or :math:`\tau_{zy}` in :math:`[M^{1} L^{-1} t^{-2}]`
    """

    def __init__(self, **kwargs):
        super(_ShearStress, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_ShearStress.transverse,
                               _ShearStress.shear,
                               **kwargs)

    @staticmethod
    @citation(_source_roloff)
    def shear(F_s, A):
        r"""
        Shear stress calculations either :math:`\tau_{sm}` in :math:`[M^{1} L^{-1} t^{-2}]`

        .. math::
           \tau_{sm} = \frac{F_s}{A}

        Args:
            F_s (:class:`~pint:.Quantity`): Shear force :math:`F_s` in :math:`[L^{1} M^{1} t^{-2}]`
            A (:class:`~pint:.Quantity`): Surface :math:`A` in :math:`[L^{2}]`

        Returns:
            :class:`~pint:.Quantity`: Shear stress :math:`\tau_{sm}` in :math:`[M^{1} L^{-1} t^{-2}]`
        """
        return F_s / A

    @staticmethod
    @citation(_source_roloff)
    def transverse(F_c, H_x, I_x, w):
        r"""
        Traverse shear stress calculations :math:`\tau_{zy}` in :math:`[M^{1} L^{-1} t^{-2}]`

        .. math::
           \tau_{zy} = \frac{F_c(z) H_x(y)}{I_x w(y)} \text{, where } H_x(y) = \int_A y dA \text{ and } I_x = \int_A
            y^2 dA

        Args:
            F_c (:class:`~pint:.Quantity`): Compression force :math:`F_c` in :math:`[L^{1} M^{1} t^{-2}]`
            H_x (:class:`~pint:.Quantity`): Height :math:`H_x` in :math:`[L^{1}]`
            I_x (:class:`~pint:.Quantity`): Moment of inertia :math:`I_x` in :math:`[L^{4}]`
            w (:class:`~pint:.Quantity`): Width :math:`w` in :math:`[L^{1}]`

        Returns:
            :class:`~pint:.Quantity`: Shear stress :math:`\tau_{zy}` in :math:`[M^{1} L^{-1} t^{-2}]`
        """
        return F_c * H_x / (I_x * w)


class _BendingStress(MTIfunc):
    r"""
    Bending stress calculations :math:`\sigma_b` in :math:`[M^{1} L^{-1} t^{-2}]`

    .. math::
       \sigma_b = \frac{M_b}{S_x} \text{, where } S_x = \frac{I_x}{e}

    or

    .. math::
       \sigma_b = \frac{M_b e}{I_x} \text{, where } I_x = \int_A y^2 dA

    Args:
        M_b (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[L^{2} M^{1} t^{-2}]`
        S_x (:class:`~pint:.Quantity`): Section modulus along x-axis :math:`S_x` in :math:`[L^{3}]`
        I_x (:class:`~pint:.Quantity`): Moment of inertia :math:`I_x` in :math:`[L^{4}]`
        e (:class:`~pint:.Quantity`): Distance from neutral axis :math:`e` in :math:`[L^{1}`

    Kwargs combinations:
        * M_b, S_x
        * M_b, I_x, e

    Returns:
        :class:`~pint:.Quantity`: Bending stress :math:`\sigma_b` in :math:`[M^{1} L^{-1} t^{-2}]`
    """

    def __init__(self, **kwargs):
        super(_BendingStress, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_BendingStress.inertia,
                               _BendingStress.section_modulus,
                               **kwargs)

    @staticmethod
    @citation(_source_roloff)
    def inertia(M_b, e, I_x):
        r"""
        Bending stress calculations :math:`\sigma_b` in :math:`[M^{1} L^{-1} t^{-2}]`

        .. math::
           \sigma_b = \frac{M_b e}{I_x} \text{, where } I_x = \int_A y^2 dA

        Args:
            M_b (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[L^{2} M^{1} t^{-2}]`
            I_x (:class:`~pint:.Quantity`): Moment of inertia :math:`I_x` in :math:`[L^{4}]`
            e (:class:`~pint:.Quantity`): Distance from neutral axis :math:`e` in :math:`[L^{1}`

        Returns:
            :class:`~pint:.Quantity`: Bending stress :math:`\sigma_b` in :math:`[M^{1} L^{-1} t^{-2}]`
        """
        return M_b * e / I_x

    @staticmethod
    @citation(_source_roloff)
    def section_modulus(M_b, S_x):
        r"""
        Bending stress calculations :math:`\sigma_b` in :math:`[M^{1} L^{-1} t^{-2}]`

        .. math::
           \sigma_b = \frac{M_b}{S_x} \text{, where } S_x = \frac{I_x}{e}

        Args:
            M_b (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[L^{2} M^{1} t^{-2}]`
            S_x (:class:`~pint:.Quantity`): Section modulus along x-axis :math:`S_x` in :math:`[L^{3}]`

        Returns:
            :class:`~pint:.Quantity`: Bending stress :math:`\sigma_b` in :math:`[M^{1} L^{-1} t^{-2}]`
        """
        return M_b / S_x


class _Stress(MTIfunc):
    r"""
    Stress calculations either :math:`\sigma` or :math:`\tau` both in :math:`[M^{1} L^{-1} t^{-2}]`

    **Tension**

    .. math::
       \sigma_t = \frac{F_t}{A}

    .. figure:: resources/tension.png
       :align: center
       :height: 200

        Source :cite:`roloff_2011`

    **Compression**

    .. math::
       \sigma_c = \frac{F_c}{A}

    .. figure:: resources/compression.png
       :align: center
       :height: 200

        Source :cite:`roloff_2011`

    **Bending**

    .. math::
       \sigma_b = \frac{M_b}{S_x} \text{, where } S_x = \frac{I_x}{e}

    or

    .. math::
       \sigma_b = \frac{M_b e}{I_x} \text{, where } I_x = \int_A y^2 dA

    .. figure:: resources/bending.png
       :align: center
       :height: 200

        Source :cite:`roloff_2011`

    **Torsion**

    .. math::
       \tau_t = \frac{T}{S_t}

    .. figure:: resources/torsion.png
       :align: center
       :height: 200

        Source :cite:`roloff_2011`

    **Shear**

    .. math::
       \tau_{sm} = \frac{F_s}{A}

    .. figure:: resources/shear.png
       :align: center
       :height: 200

        Source :cite:`roloff_2011`

    **Traverse shear**

    .. math::
       \tau_{zy} = \frac{F_c(z) H_x(y)}{I_x w(y)} \text{, where } H_x(y) = \int_A y dA \text{ and } I_x = \int_A y^2 dA

    .. figure:: resources/traverse.png
       :align: center
       :height: 200

        Source :cite:`roloff_2011`

    Args:
        F_t (:class:`~pint:.Quantity`): Tension force :math:`F_t` in :math:`[L^{1} M^{1} t^{-2}]`
        F_c (:class:`~pint:.Quantity`): Compression force :math:`F_c` in :math:`[L^{1} M^{1} t^{-2}]`
        F_s (:class:`~pint:.Quantity`): Shear force :math:`F_s` in :math:`[L^{1} M^{1} t^{-2}]`
        T (:class:`~pint:.Quantity`): Torque :math:`T` in :math:`[L^{2} M^{1} t^{-2}]`
        M_b (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[L^{2} M^{1} t^{-2}]`
        A (:class:`~pint:.Quantity`): Surface :math:`A` in :math:`[L^{2}]`
        S_x (:class:`~pint:.Quantity`): Section modulus along x-axis :math:`S_x` in :math:`[L^{3}]`
        S_t (:class:`~pint:.Quantity`): Section modulus torque :math:`S_t` in :math:`[L^{3}]`
        H_x (:class:`~pint:.Quantity`): Height :math:`H_x` in :math:`[L^{1}]`
        I_x (:class:`~pint:.Quantity`): Moment of inertia :math:`I_x` in :math:`[L^{4}]`
        e (:class:`~pint:.Quantity`): Distance from neutral axis :math:`e` in :math:`[L^{1}`
        w (:class:`~pint:.Quantity`): Width :math:`w` in :math:`[L^{1}]`

    Kwargs combinations:
        * F_t, A
        * F_c, A
        * M_b, S_x
        * M_b, I_x, e
        * F_s, A
        * F_c, H_x, I_x, w
        * T, S_t

    Returns:
        :class:`~pint:.Quantity`: Stress :math:`\sigma` or :math:`\tau` in :math:`[M^{1} L^{-1} t^{-2}]`

    Todo:
        Correct dimensions for traverse shearstress
    """

    def __init__(self, **kwargs):
        super(_Stress, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_Stress.tension,
                               _Stress.compression,
                               _Stress.torsion,
                               _BendingStress.section_modulus,
                               _BendingStress.inertia,
                               _ShearStress.shear,
                               _ShearStress.transverse,
                               **kwargs)

    @staticmethod
    @citation(_source_roloff)
    def tension(F_t, A):
        r"""
        Tension stress calculations :math:`\sigma_t` in :math:`[M^{1} L^{-1} t^{-2}]`

        .. math::
           \sigma_t = \frac{F_t}{A}

        Args:
            F_t (:class:`~pint:.Quantity`): Tension force :math:`F_t` in :math:`[L^{1} M^{1} t^{-2}]`
            A (:class:`~pint:.Quantity`): Surface :math:`A` in :math:`[L^{2}]`

        Returns:
            :class:`~pint:.Quantity`: Tension stress :math:`\sigma_t` in :math:`[M^{1} L^{-1} t^{-2}]`
        """
        return F_t / A

    @staticmethod
    @citation(_source_roloff)
    def compression(F_c, A):
        r"""
        Compression stress calculations :math:`\sigma_t` in :math:`[M^{1} L^{-1} t^{-2}]`

        .. math::
           \sigma_c = \frac{F_c}{A}

        Args:
            F_t (:class:`~pint:.Quantity`): Compression force :math:`F_t` in :math:`[L^{1} M^{1} t^{-2}]`
            A (:class:`~pint:.Quantity`): Surface :math:`A` in :math:`[L^{2}]`

        Returns:
            :class:`~pint:.Quantity`: Compression stress :math:`\sigma_t` in :math:`[M^{1} L^{-1} t^{-2}]`
        """
        return F_c / A

    @staticmethod
    @citation(_source_roloff)
    def torsion(T, S_t):
        r"""
        Torsion stress calculations :math:`\tau_t` in :math:`[M^{1} L^{-1} t^{-2}]`

        .. math::
           \tau_t = \frac{T}{S_t}

        Args:
            T (:class:`~pint:.Quantity`): Torque :math:`T` in :math:`[L^{2} M^{1} t^{-2}]`
            S_t (:class:`~pint:.Quantity`): Section modulus torque :math:`S_t` in :math:`[L^{3}]`

        Returns:
            :class:`~pint:.Quantity`: Torsion stress calculations :math:`\tau_t` in :math:`[M^{1} L^{-1} t^{-2}]`
        """
        return T / S_t

    bending = _BendingStress()

    shear = _ShearStress()


stress = _Stress()


class _Power(MTIfunc):
    r"""
    Power :math:`P` in :math:`[L^{2} M^{1} t^{-3}]`
    When a certain force is applied at a certain speed, the following equation is used:

    .. math::
       P = F v

    or for a rotational torque applied at a certain rotational speed:

    .. math::
       P = T \omega

    Args:
        T (:class:`~pint:.Quantity`): Torque :math:`T` in :math:`[M^{1} L^{2} t^{-2}]`
        F (:class:`~pint:.Quantity`): Force :math:`F` in :math:`[M^{1} L^{1} t^{-2}]`
        omega (:class:`~pint:.Quantity`): Rotational speed :math:`\omega` in :math:`[t^{-1}]`
        v (:class:`~pint:.Quantity`): Speed :math:`v` in :math:`[L^{1} t^{-1}]`

    Kwargs combinations:
        * T, omega
        * F, v

    Returns:
        :class:`~pint:.Quantity`: Power :math:`P` in :math:`[L^{2} M^{1} t^{-3}]`
"""

    def __init__(self, **kwargs):
        super(_Power, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_Power.linear_work,
                               _Power.rotational_work,
                               **kwargs)

    @staticmethod
    def linear_work(F, v):
        r"""
        Power :math:`P` in :math:`[L^{2} M^{1} t^{-3}]`
        For a certain force applied at a certain speed

        .. math::
           P = F v

        Args:
            F (:class:`~pint:.Quantity`): Force :math:`F` in :math:`[M^{1} L^{1} t^{-2}]`
            v (:class:`~pint:.Quantity`): Speed :math:`v` in :math:`[L^{1} t^{-1}]`

        Returns:
            :class:`~pint:.Quantity`: Power :math:`P` in :math:`[L^{2} M^{1} t^{-3}]`
        """
        return F * v

    @staticmethod
    def rotational_work(T, omega):
        r"""
        Power :math:`P` in :math:`[L^{2} M^{1} t^{-3}]` for a rotational torque applied at a certain rotational speed:

    .. math::
       P = T \omega

    Args:
        T (:class:`~pint:.Quantity`): Torque :math:`T` in :math:`[M^{1} L^{2} t^{-2}]`
        omega (:class:`~pint:.Quantity`): Rotational speed :math:`\omega` in :math:`[t^{-1}]`

        Returns:
            :class:`~pint:.Quantity`: Power :math:`P` in :math:`[L^{2} M^{1} t^{-3}]`
        """
        return T * omega


power = _Power()


class _Torque(MTIfunc):
    r"""
    calculate torque :math:`T` in :math:`[M^{1} L^{2} t^{-2}]`

    .. math::
       T = \frac{P}{\omega}

    Args:
        P (:class:`~pint:.Quantity`): Power :math:`P` in :math:`[L^{2} M^{1} t^{-3}]`
        omega (:class:`~pint:.Quantity`): Rotational speed :math:`\omega` in :math:`[t^{-1}]`

    Kwargs combinations:
        * P, omega

    Returns:
        :class:`~pint:.Quantity`: Torque :math:`T` in :math:`[M^{1} L^{2} t^{-2}]`
    """

    def __init__(self, **kwargs):
        super(_Torque, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_Torque.rotation,
                               **kwargs)

    @staticmethod
    def rotation(P, omega):
        r"""
        calculate torque :math:`T` in :math:`[M^{1} L^{2} t^{-2}]`

        .. math::
           T = \frac{P}{\omega}

        Args:
            P (:class:`~pint:.Quantity`): Power :math:`P` in :math:`[L^{2} M^{1} t^{-3}]`
            omega (:class:`~pint:.Quantity`): Rotational speed :math:`\omega` in :math:`[t^{-1}]`

        Returns:
            :class:`~pint:.Quantity`: Torque :math:`T` in :math:`[M^{1} L^{2} t^{-2}]`
        """
        return P / omega


torque = _Torque()


@citation(_source_roloff)
def equivalent_torque(T_nom, K_A):
    r"""
    Equivalent torque :math:`T_{eq}` in :math:`[M^{1} L^{2} t^{-2}]`

    .. math::
       T_{eq} = K_A T_{nom}

    Args:
        T_nom (:class:`~pint:.Quantity`): Nominal torque :math:`T_{nom}` in :math:`[M^{1} L^{2} t^{-2}]`
        K_A (:class:`~pint:.Quantity`): Application factor :math:`K_A` in :math:`[-]` according to Roloff/Matek table
         3-5 or NEN-ISO 6336-1 or obtained with the function :func:`.application_factor`

    Returns:
        :class:`~pint:.Quantity`: Equivalent torque :math:`T_{eq}` in :math:`[M^{1} L^{2} t^{-2}]`
    """
    return K_A * T_nom


class _EquivalentMoment(MTIfunc):
    r"""
    Equivalent moment :math:`M_v` in :math:`[M^{1} L^{2} t^{-2}]` for an axle subject to an moment and/or torque.

    .. math::
       M_v = \sqrt{M^2 + \left(\frac{\bar{\sigma}_{b}}{2 \bar{\tau}_{t}} T\right)^2}

    or

    .. math::
       M_v = \sqrt{M^2 + 0.75 \left(\frac{\bar{\sigma}_{b}}{\varphi \bar{\tau}_{t}} T\right)^2}

    or when a torque is known but there is also an unknown moment

    .. math::
       M_v = \varphi T \text{, where } 1.17 \leq \varphi \leq 2.1

    Args:
        M (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
        T (:class:`~pint:.Quantity`): Torque :math:`T` in :math:`[M^{1} L^{2} t^{-2}]`
        sigma_b (:class:`~pint:.Quantity`): Allowable flexural strength bending :math:`\bar{\sigma}_{b}`
         in :math:`[M^{1} L^{3} t^{-2}]`
        tau_t (:class:`~pint:.Quantity`): Allowable flexural strength for torsion :math:`\bar{\tau}_{t}`
         in :math:`[M^{1} L^{3} t^{-2}]`
        phi (:class:`~pint:.Quantity`): load factor :math:`\varphi` in :math:`[-]`

    Kwargs combinations:
        * M, T, sigma_b, tau_t
        * M, T, sigma_b, tau_t, phi
        * T, phi

    Returns:
        :class:`~pint:.Quantity`: Equivalent moment :math:`M_v` in :math:`[M^{1} L^{2} t^{-2}]`
    """

    def __init__(self, **kwargs):
        super(_EquivalentMoment, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_EquivalentMoment.equivalent,
                               _EquivalentMoment.equivalent_factor,
                               _EquivalentMoment.torque,
                               **kwargs)

    @staticmethod
    @citation(_source_roloff)
    def equivalent(M, T, sigma_b, tau_t):
        r"""
        Equivalent moment :math:`M_v` in :math:`[M^{1} L^{2} t^{-2}]` for an axle subject to a moment and torque.

        .. math::
           M_v = \sqrt{M^2 + \left(\frac{\bar{\sigma}_{b}}{2 \bar{\tau}_{t}} T\right)^2}

        Args:
            M (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
            T (:class:`~pint:.Quantity`): Torque :math:`T` in :math:`[M^{1} L^{2} t^{-2}]`
            sigma_b (:class:`~pint:.Quantity`): Allowable flexural strength bending :math:`\bar{\sigma}_{b}`
             in :math:`[M^{1} L^{3} t^{-2}]`
            tau_t (:class:`~pint:.Quantity`): Allowable flexural strength for torsion :math:`\bar{\tau}_{t}`
             in :math:`[M^{1} L^{3} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Equivalent moment :math:`M_v` in :math:`[M^{1} L^{2} t^{-2}]`
        """
        return (M ** 2 + (sigma_b * T / (2 * tau_t)) ** 2) ** (1 / 2)

    @staticmethod
    @citation(_source_roloff)
    def equivalent_factor(M, T, sigma_b, tau_t, phi):
        r"""
        Equivalent moment :math:`M_v` in :math:`[M^{1} L^{2} t^{-2}]` for an axle subject to a moment and torque.

        .. math::
           M_v = \sqrt{M^2 + 0.75 \left(\frac{\bar{\sigma}_{b}}{\varphi \bar{\tau}_{t}} T\right)^2}

        Args:
            M (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
            T (:class:`~pint:.Quantity`): Torque :math:`T` in :math:`[M^{1} L^{2} t^{-2}]`
            sigma_b (:class:`~pint:.Quantity`): Allowable flexural strength bending :math:`\bar{\sigma}_{b}`
             in :math:`[M^{1} L^{3} t^{-2}]`
            tau_t (:class:`~pint:.Quantity`): Allowable flexural strength for torsion :math:`\bar{\tau}_{t}`
             in :math:`[M^{1} L^{3} t^{-2}]`
            phi (:class:`~pint:.Quantity`): load factor :math:`\varphi` in :math:`[-]`

        Returns:
            :class:`~pint:.Quantity`: Equivalent moment :math:`M_v` in :math:`[M^{1} L^{2} t^{-2}]`
        """
        return (M ** 2 + 0.75 * (sigma_b * T / (phi * tau_t)) ** 2) ** (1 / 2)

    @staticmethod
    @citation(_source_roloff)
    def torque(T, phi):
        r"""
        Equivalent moment :math:`M_v` in :math:`[M^{1} L^{2} t^{-2}]` for an axle subject to an unknown moment and a
        known torque.

        .. math::
           M_v = \varphi T

        Args:
            T (:class:`~pint:.Quantity`): Torque :math:`T` in :math:`[M^{1} L^{2} t^{-2}]`
            phi (:class:`~pint:.Quantity`): load factor :math:`\varphi` in :math:`[-]` dependent on the support distance
             value should fall between :math:`1.17 ... 2.1`

        Returns:
            :class:`~pint:.Quantity`: Equivalent moment :math:`M_v` in :math:`[M^{1} L^{2} t^{-2}]`
        """
        return T * phi


equivalent_moment = _EquivalentMoment()


class _StaticAllowableStress(MTIfunc):
    r"""
    Allowable stress :math:`\bar{\sigma}_{t}` in :math:`[M^{1} L^{-1} t^{-2}]`

    **Ductile materials**

    .. math::
       \bar{\sigma}_{t} = \frac{R_{eN}}{S_{V,min}}

    **Brittle materials**

    .. math::
       \bar{\sigma}_{t} = \frac{R_{mN}}{S_{B,min}}

    Args:
        R_eN (:class:`~pint:.Quantity`): Yield strength :math:`R_{eN}` in :math:`[M^{1} L^{-1} t^{-2}]`
        R_mN (:class:`~pint:.Quantity`): Ultimate tensile strength :math:`R_{mN}` in :math:`[M^{1} L^{-1} t^{-2}]`
        S_V_min (:class:`~pint:.Quantity`): Minimum yield safety factor :math:`S_{V,min}` in :math:`[-]`, value
         should fall between :math:`1.2 ... 1.8`
        S_B_min (:class:`~pint:.Quantity`): Minimum tensile safety factor :math:`S_{B,min}` in :math:`[-]`, value
         should fall between :math:`1.5 ... 3.0`

    Kwargs combinations:
        * R_eN, S_V_min
        * R_mN, S_B_min

    Returns:
        :class:`~pint:.Quantity`: Allowable stress :math:`\bar{\sigma}_{t}` in :math:`[M^{1} L^{-1} t^{-2}]`

    Raises:
        ValueError: When S_V_min or S_B_min is outside bounds
    """

    def __init__(self, **kwargs):
        super(_StaticAllowableStress, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_StaticAllowableStress.ductile,
                               _StaticAllowableStress.brittle,
                               **kwargs)

    @staticmethod
    @citation(_source_roloff)
    def ductile(R_eN, S_V_min):
        r"""
        Allowable stress :math:`\bar{\sigma}_{t}` for ductile materials in :math:`[M^{1} L^{-1} t^{-2}]`

        .. math::
           \bar{\sigma}_{t} = \frac{R_{eN}}{S_{V,min}}

        Args:
            R_eN (:class:`~pint:.Quantity`): Yield strength :math:`R_{eN}` in :math:`[M^{1} L^{-1} t^{-2}]`
            S_V_min (:class:`~pint:.Quantity`): Minimum yield safety factor :math:`S_{V,min}` in :math:`[-]`, value
             should fall between :math:`1.2 ... 1.8`

        Returns:
            :class:`~pint:.Quantity`: Allowable stress :math:`\bar{\sigma}_{t}` in :math:`[M^{1} L^{-1} t^{-2}]`

        Raises:
            ValueError: When S_V_min is outside bounds
        """
        if isiterable(S_V_min):
            if not all([1.2 <= s <= 1.8 for s in S_V_min]):
                raise ValueError('S_V_min should fall between 1.2 ... 1.8')
        else:
            if not 1.2 <= S_V_min <= 1.8:
                raise ValueError('S_V_min should fall between 1.2 ... 1.8')
        return R_eN / S_V_min

    @staticmethod
    @citation(_source_roloff)
    def brittle(R_mN, S_B_min):
        r"""
        Allowable stress :math:`\bar{\sigma}_{t}` for brittle materials in :math:`[M^{1} L^{-1} t^{-2}]`

        .. math::
           \bar{\sigma}_{t} = \frac{R_{mN}}{S_{B,min}}

        Args:
            R_mN (:class:`~pint:.Quantity`): Ultimate tensile strength :math:`R_{mN}` in :math:`[M^{1} L^{-1} t^{-2}]`
            S_B_min (:class:`~pint:.Quantity`): Minimum tensile safety factor :math:`S_{B,min}` in :math:`[-]`, value
             should fall between :math:`1.5 ... 3.0`

        Returns:
            :class:`~pint:.Quantity`: Allowable stress :math:`\bar{\sigma}_{t}` in :math:`[M^{1} L^{-1} t^{-2}]`

        Raises:
            ValueError: When S_B_min is outside bounds
        """
        if isiterable(S_B_min):
            if not all([1.5 <= s <= 3. for s in S_B_min]):
                raise ValueError('S_B_min should fall between 1.5 ... 3.0')
        else:
            if not 1.5 <= S_B_min <= 3.0:
                raise ValueError('S_B_min should fall between 1.5 ... 3.0')
        return R_mN / S_B_min


class _DynamicAllowableStress(MTIfunc):
    r"""
    Allowable dynamic stress :math:`\bar{\tau}` or :math:`\bar{\sigma}` in :math:`[M^{1} L^{-1} t^{-2}]`

    **Bending load**

    .. math::
       \bar{\sigma} = \frac{\sigma_{D}}{S_{D,min}}

    **Torque load**

    .. math::
       \bar{\tau} = \frac{\tau_D}{S_{D,min}}

    Args:
        sigma_D (:class:`~pint:.Quantity`): Bending stress :math:`\sigma_D` in :math:`[M^{1} L^{-1} t^{-2}]`.
         Dependent on the load type :math:`\sigma_D = \sigma_W` or, if the load is of a swelling type:
         :math:`\sigma_D = \sigma_{zW}`.
        tau_D (:class:`~pint:.Quantity`): Torque stress :math:`\tau_D` in :math:`[M^{1} L^{-1} t^{-2}]`.
         Dependent on the load type :math:`\tau_D = \tau_W` or, if the load is of a swelling type:
         :math:`\tau_D = \tau_zW`.
        S_D_min (:class:`~pint:.Quantity`): Minimum dynamic yield safety factor :math:`S_{D,min}` in :math:`[-]`,
         value should fall between :math:`3.0 ... 4.0`.

    Kwargs combinations:
        * sigma_D, S_D_min
        * tau_D, S_D_min

    Returns:
        :class:`~pint:.Quantity`: Allowable stress :math:`\bar{\sigma}` or :math:`\bar{\tau}` in
         :math:`[M^{1} L^{-1} t^{-2}]`

    Raises:
        ValueError: When S_D_min is outside bounds
    """

    def __init__(self, **kwargs):
        super(_DynamicAllowableStress, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_DynamicAllowableStress.moment,
                               _DynamicAllowableStress.torque,
                               **kwargs)

    @staticmethod
    @citation(_source_roloff)
    def moment(sigma_D, S_D_min):
        r"""
        Allowable bending stress :math:`\bar{\sigma}` for a dynamic load in :math:`[M^{1} L^{-1} t^{-2}]`

        .. math::
           \bar{\sigma} = \frac{\sigma_{D}}{S_{D,min}}

        Args:
            sigma_D (:class:`~pint:.Quantity`): Bending stress :math:`\sigma_D` in :math:`[M^{1} L^{-1} t^{-2}]`.
             Dependent on the load type :math:`\sigma_D = \sigma_W` or, if the load is of a swelling type:
             :math:`\sigma_D = \sigma_{zW}`.
            S_D_min (:class:`~pint:.Quantity`): Minimum dynamic yield safety factor :math:`S_{D,min}` in :math:`[-]`,
             value should fall between :math:`3.0 ... 4.0`.

        Returns:
            :class:`~pint:.Quantity`: Allowable stress :math:`\bar{\sigma}` in :math:`[M^{1} L^{-1} t^{-2}]`

        Raises:
            ValueError: When S_D_min is outside bounds
        """
        if isiterable(S_D_min):
            if not all([3. <= s <= 4. for s in S_D_min]):
                raise ValueError('S_D_min should fall between 3.0 ... 4.0')
        else:
            if not 3.0 <= S_D_min <= 4.0:
                raise ValueError('S_D_min should fall between 3.0 ... 4.0')
        return sigma_D / S_D_min

    @staticmethod
    @citation(_source_roloff)
    def torque(tau_D, S_D_min):
        r"""
        Allowable torque stress :math:`\bar{\tau}` for a dynamic load in :math:`[M^{1} L^{-1} t^{-2}]`

        .. math::
           \bar{\tau} = \frac{\tau_D}{S_{D,min}}

        Args:
            tau_D (:class:`~pint:.Quantity`): Torque stress :math:`\tau_D` in :math:`[M^{1} L^{-1} t^{-2}]`.
             Dependent on the load type :math:`\tau_D = \tau_W` or, if the load is of a swelling type:
             :math:`\tau_D = \tau_zW`.
            S_D_min (:class:`~pint:.Quantity`): Minimum dynamic yield safety factor :math:`S_{D,min}` in :math:`[-]`,
             value should fall between :math:`3.0 ... 4.0`.

        Returns:
            :class:`~pint:.Quantity`: Allowable stress :math:`\bar{\tau}` in :math:`[M^{1} L^{-1} t^{-2}]`

        Raises:
            ValueError: When S_D_min is outside bounds
        """
        if isiterable(S_D_min):
            if not all([3. <= s <= 4. for s in S_D_min]):
                raise ValueError('S_D_min should fall between 3.0 ... 4.0')
        else:
            if not 3.0 <= S_D_min <= 4.0:
                raise ValueError('S_D_min should fall between 3.0 ... 4.0')
        return tau_D / S_D_min


class _AllowableStress(MTIfunc):
    r"""
    Allowable stress for dynamic and static loads :math:`\bar{\tau}` or :math:`\bar{\sigma}` in
    :math:`[M^{1} L^{-1} t^{-2}]`

    **Ductile materials with a static load**

    .. math::
       \bar{\sigma}_{t} = \frac{R_{eN}}{S_{V,min}}

    **Brittle materials with a static load**

    .. math::
       \bar{\sigma}_{t} = \frac{R_{mN}}{S_{B,min}}

    **Dynamic Bending load**

    .. math::
       \bar{\sigma} = \frac{\sigma_{D}}{S_{D,min}}

    **Dynamic Torque load**

    .. math::
       \bar{\tau} = \frac{\tau_D}{S_{D,min}}

    Args:
        R_eN (:class:`~pint:.Quantity`): Yield strength :math:`R_{eN}` in :math:`[M^{1} L^{-1} t^{-2}]`
        R_mN (:class:`~pint:.Quantity`): Ultimate tensile strength :math:`R_{mN}` in :math:`[M^{1} L^{-1} t^{-2}]`
        sigma_D (:class:`~pint:.Quantity`): Bending stress :math:`\sigma_D` in :math:`[M^{1} L^{-1} t^{-2}]`.
         Dependent on the load type :math:`\sigma_D = \sigma_W` or, if the load is of a swelling type:
         :math:`\sigma_D = \sigma_{zW}`.
        tau_D (:class:`~pint:.Quantity`): Torque stress :math:`\tau_D` in :math:`[M^{1} L^{-1} t^{-2}]`.
         Dependent on the load type :math:`\tau_D = \tau_W` or, if the load is of a swelling type:
         :math:`\tau_D = \tau_zW`.
        S_V_min (:class:`~pint:.Quantity`): Minimum yield safety factor :math:`S_{V,min}` in :math:`[-]`, value
         should fall between :math:`1.2 ... 1.8`
        S_B_min (:class:`~pint:.Quantity`): Minimum tensile safety factor :math:`S_{B,min}` in :math:`[-]`, value
         should fall between :math:`1.5 ... 3.0`
        S_D_min (:class:`~pint:.Quantity`): Minimum dynamic yield safety factor :math:`S_{D,min}` in :math:`[-]`,
         value should fall between :math:`3.0 ... 4.0`.

    Kwargs combinations:
        * R_eN, S_V_min
        * R_mN, S_B_min
        * sigma_D, S_D_min
        * tau_D, S_D_min

    Returns:
        :class:`~pint:.Quantity`: Allowable stress :math:`\bar{\sigma}` or :math:`\bar{\tau}` in
         :math:`[M^{1} L^{-1} t^{-2}]`

    Raises:
        ValueError: When S_D_min, S_V_min or S_B_min is outside bounds
    """

    def __init__(self, **kwargs):
        super(_AllowableStress, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_StaticAllowableStress.brittle,
                               _StaticAllowableStress.ductile,
                               _DynamicAllowableStress.moment,
                               _DynamicAllowableStress.torque,
                               **kwargs)

    static = _StaticAllowableStress()

    dynamic = _DynamicAllowableStress()


allowable_stress = _AllowableStress()


class _YieldSafetyFactor(MTIfunc):
    r"""
    Yield safety factor :math:`S` in :math:`[-]`

    **Static**

    .. math::
       S_v = \frac{1}{\sqrt{\left(\frac{\sigma_{b,max}}{\sigma_{bv}}\right)^2 + \left(\frac{\tau_{t,max}}{\tau_{
       tv}}\right)^2}}

    **Dynamic**

    .. math::
       S_D = \frac{1}{\sqrt{\left(\frac{\sigma_{b,a}}{\sigma_{GA}}\right)^2 + \left(\frac{\tau_{t,a}}{\tau_{GA}}
       \right)^2}}

    Args:
        sigma_b_a (:class:`~pint:.Quantity`): Amplitude of the nominal bending stress :math:`\sigma_{b,a}` in
         :math:`[M^{1} L^{-1} t^{-2}]`
        sigma_b_max (:class:`~pint:.Quantity`): Maximum bending stress :math:`\sigma_{b,max}` in
         :math:`[M^{1} L^{-1} t^{-2}]`
        sigma_GA (:class:`~pint:.Quantity`): Amplitude bending strength of part :math:`\sigma_{GA}` in
         :math:`[M^{1} L^{-1} t^{-2}]`
        sigma_bv (:class:`~pint:.Quantity`): Bending yield limit :math:`\sigma_{bv}` in
         :math:`[M^{1} L^{-1} t^{-2}]`
        tau_t_a (:class:`~pint:.Quantity`): Amplitude of the nominal torsion stress :math:`\tau_{t,a}` in
         :math:`[M^{1} L^{-1} t^{-2}]`
        tau_t_max (:class:`~pint:.Quantity`): Maximum torsion stress :math:`\tau_{t,max}` in
         :math:`[M^{1} L^{-1} t^{-2}]`
        tau_GA (:class:`~pint:.Quantity`): Amplitude torsion strength of part :math:`\tau_{GA}` in
         :math:`[M^{1} L^{-1} t^{-2}]`
        tau_tv (:class:`~pint:.Quantity`): Torsion yield limit :math:`\tau_{tv}` in :math:`[M^{1} L^{-1} t^{-2}]`


    Kwargs combinations:
        * sigma_b_max, sigma_bv, tau_t_max, tau_tv
        * sigma_b_a, sigma_GA, tau_t_a, tau_GA

    Returns:
        :class:`~pint:.Quantity`: Yield safety factor :math:`S` in :math:`[-]`
    """

    def __init__(self, **kwargs):
        super(_YieldSafetyFactor, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_YieldSafetyFactor.static,
                               _YieldSafetyFactor.dynamic,
                               **kwargs)

    @staticmethod
    @citation(_source_roloff)
    def static(sigma_b_max, sigma_bv, tau_t_max, tau_tv):
        r"""
        Yield safety factor :math:`S_V` in :math:`[-]` for a static situation

        .. math::
           S_v = \frac{1}{\sqrt{\left(\frac{\sigma_{b,max}}{\sigma_{bv}}\right)^2 + \left(\frac{\tau_{t,max}}{\tau_{
           tv}}\right)^2}}

        Args:
            sigma_b_max (:class:`~pint:.Quantity`): Maximum bending stress :math:`\sigma_{b,max}` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            sigma_bv (:class:`~pint:.Quantity`): Bending yield limit :math:`\sigma_{bv}` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            tau_t_max (:class:`~pint:.Quantity`): Maximum torsion stress :math:`\tau_{t,max}` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            tau_tv (:class:`~pint:.Quantity`): Torsion yield limit :math:`\tau_{tv}` in :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Yield safety factor :math:`S_{V}` in :math:`[-]`
        """
        return 1 / ((sigma_b_max / sigma_bv) ** 2 + (tau_t_max / tau_tv) ** 2) ** (1 / 2)

    @staticmethod
    @citation(_source_roloff)
    def dynamic(sigma_b_a, sigma_GA, tau_t_a, tau_GA):
        r"""
        Yield safety factor :math:`S_D` in :math:`[-]` for a dynamic situation

        .. math::
           S_D = \frac{1}{\sqrt{\left(\frac{\sigma_{b,a}}{\sigma_{GA}}\right)^2 + \left(\frac{\tau_{t,a}}{\tau_{GA}}
           \right)^2}}

        Args:
            sigma_b_a (:class:`~pint:.Quantity`): Amplitude of the nominal bending stress :math:`\sigma_{b,a}` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            sigma_GA (:class:`~pint:.Quantity`): Amplitude bending strength of part :math:`\sigma_{GA}` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            tau_t_a (:class:`~pint:.Quantity`): Amplitude of the nominal torsion stress :math:`\tau_{t,a}` in
             :math:`[M^{1} L^{-1} t^{-2}]`
            tau_GA (:class:`~pint:.Quantity`): Amplitude torsion strength of part :math:`\tau_{GA}` in
             :math:`[M^{1} L^{-1} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Yield safety factor :math:`S_{D}` in :math:`[-]`
        """
        return 1 / ((sigma_b_a / sigma_GA) ** 2 + (tau_t_a / tau_GA) ** 2) ** (1 / 2)


yield_safety_factor = _YieldSafetyFactor()
