r"""
.. module:: MTIpython.mech.auger
    :platform: Unix, Windows
    :synopsis: Auger component module

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from enum import Enum

from MTIpython.core.binder import Binder, Valid
from MTIpython.core.mtiobj import MTIobj
from MTIpython.material import Powder
from MTIpython.mtimath.geometry import A_circle
from MTIpython.mtimath.types import inf
from MTIpython.units import u


class Filling(Enum):
    r"""
    Filling coefficient, used in Auger calculations source: NEN-ISO 7119:1982
    """

    EASY = 0.45
    r"""
    Materials which flow easily, scarcely abrasive (flour, cereals) source: NEN-ISO 7119:1982 """

    AVERAGE = 0.3
    r"""
    Matter with average abrasive properties, with a grading varying from grains to small lumps (salt, 
    sand, coal) source: NEN-ISO 7119:1982"""

    HEAVY = 0.15
    r"""
    For heavy bulk materials, very abrasive, aggressive (ash, sand, minerals) source: NEN-ISO 7119:1982 """


class Auger(MTIobj):
    def __init__(self, medium=None, D=None, L=None, S=None, phi=Filling.AVERAGE, H=None, n=None, **kwargs):
        r"""
        Transport Auger calculations according to NEN-ISO 7119:1982.

        Args:
            medium (:class:`MTIpython.material.granular.Granular`): Granular material to be transported
            D (:class:`~pint:.Quantity` or :class:`.Binder`): Nominal screw diameter :math:`D` in :math:`[L^{1}]`
            L (:class:`~pint:.Quantity` or :class:`.Binder`): Conveying length :math:`L` in :math:`[L^{1}]`
            S (:class:`~pint:.Quantity` or :class:`.Binder`): Screw pitch :math:`S` in :math:`[L^{1}]`
            phi (:class:`~Filling`): Through filling coefficient :math:`\phi` in :math:`[-]` (default)
             :attr:`~Filling.AVERAGE`
            H (:class:`~pint:.Quantity` or :class:`.Binder`): Height difference :math:`H` in :math:`[L^{1}]`
        """
        self.medium = medium
        self.D = Binder(value=D, bounds=(0., inf), unit=u.m)
        self.L = Binder(value=L, bounds=(0., inf), unit=u.m)
        self.phi = phi
        self.H = Binder(value=H, bounds=(-inf, inf), unit=u.m)
        self.n = Binder(value=n, bounds=(0., inf), unit=u.rpm)
        self.S = Binder(value=S, bounds=(0., inf), unit=u.m)
        super(Auger, self).__init__(**kwargs)

    medium: Powder = None
    """:class:`MTIpython.material.granular.Granular`: medium which is transported through the auger"""

    D = Valid()
    r""":class:`.Valid`: Nominal screw diameter :math:`D` in :math:`[L^{1}]`"""

    L = Valid()
    r""":class:`.Valid`: Conveying length :math:`L` in :math:`[L^{1}]`"""

    n = Valid()
    r""":class:`.Valid`: number of screw rotations :math:`[rpm]`"""

    S = Valid()
    r""":class:`.Valid`: Screw pitch :math:`S` in :math:`[L^{1}]`"""

    H = Valid()
    r""":class:`.Valid`: Height difference :math:`H` in :math:`[L^{1}]`"""

    phi = Filling.AVERAGE
    r""":class:`~Filling`: Through filling coefficient :math:`\phi` in :math:`[-]`"""

    @property
    def A(self):
        r"""
        Quantity: Working section of screw conveyor :math:`A` in :math:`[L^{2}]`
        """
        return self.phi.value * A_circle(D=self.D)

    @property
    def v(self):
        r"""
        Quantity: Conveying speed :math:`v` in :math:`[L^{1} t^{-1}]`
        """
        return self.S * self.n

    @v.setter
    def v(self, value):
        self.n = (value / self.S).to(u.rpm)

    @property
    def V_flux(self):
        r"""Quantity: Volume flow rate :math:`\dot{V}` in :math:`[L^{3} t^{-1}]`"""
        return self.v * self.A

    @V_flux.setter
    def V_flux(self, value):
        self.v = value / self.A

    @property
    def m_flux(self):
        r"""Quantity: Mass flow rate :math:`\dot{m}` in :math:`[M^{1} t^{-1}]`"""
        return self.V_flux * self.medium.rho

    @m_flux.setter
    def m_flux(self, value):
        self.V_flux = value / self.medium.rho

    @property
    def P(self):
        r"""Quantity: Total power :math:`P` in :math:`[L^{2} M^{1} t^{-3}]`"""
        return self.P_H + self.P_N + self.P_St

    @property
    def P_H(self):
        r"""Quantity: Power necessary for the progress of material :math:`P_{H}` in :math:`[L^{2} M^{1} t^{-3}]`"""
        return self.m_flux * self.L * self.medium.Lambda * u.g_0

    @property
    def P_N(self):
        r"""Quantity: Drive power of the screw conveyor at no load :math:`P_{N}` in :math:`[L^{2} M^{1} t^{-3}]`
        Todo: Use a future Mech package expansion for beter modeling for now definition of NEN-ISO 7119:1982 is used
        Todo: Units don't match up FIX.
        """
        return (self.D * self.L) / 20. * u.kW / u.m ** 2

    @property
    def P_St(self):
        r"""Quantity: Power necessary for the progress of material over a height :math:`P_{St}` in :math:`[L^{2} M^{
        1} t^{-3}]`"""
        return self.m_flux * self.H * u.g_0
