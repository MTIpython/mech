r"""
.. module:: MTIpython.mech.principal.gear
    :platform: Unix, Windows
    :synopsis: Core component module

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from collections import UserList

from MTIpython.core.core import Product

__all__ = ['GearingRatio']


class GearingRatio(UserList):
    def __init__(self, initlist):
        r"""
        A list where all the gearing ratio's for a gear train can be stored.

        Example usage:

            >>> gears = GearingRatio([1.5, 3., 6., 4.5, 6.])
            >>> gears.total
            729.0
            >>> gears.total / 3
            243.0
            >>> gears.total[1:3]
            18.0
            >>> gears.total[1:3] * 3
            2187.0
            >>> gears[2]
            6.0
            >>> gears[1:3]
            [3.0, 6.0]

        Args:
            initlist (:class:`~python:list` or :class:`~numpy:numpy.ndarray`): initialization list
        """
        super(GearingRatio, self).__init__(initlist=initlist)
        self.total = Product(self.data)

    total = Product(None)
    r"""
    Get the total gearing ratio. Defined as

    .. math::
       i_t = \displaystyle \prod_{n=0}^{len-1} i_n
    """
