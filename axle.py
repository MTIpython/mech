r"""
.. module:: MTIpython.mech.axle
    :platform: Unix, Windows
    :synopsis: Axle component module

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from enum import Enum

from MTIpython.core.binder import Binder, Valid
from MTIpython.core.mtiobj import MTIobj
from MTIpython.mtimath.types import inf
from MTIpython.units.SI import u

__all__ = ['Axle', 'AxleType']


class AxleType(Enum):
    r"""
    Axle type
    """
    DRIVING = 1
    LOADBEARING = 2
    CARDAN = 3


class Axle(MTIobj):
    def __init__(self, V=None, M=None, T=None, material=None, d=None, k=0.6, axle_type=AxleType.DRIVING, hollow=False,
                 **kwargs):
        self.d = Binder(value=d, bounds=(0., inf), unit=u.mm)
        self.V = Binder(value=V, unit=u.N)
        self.M = Binder(value=M, unit=u.N * u.m)
        self.T = Binder(value=T, unit=u.N * u.m)
        self.axle_type = axle_type
        self.hollow = hollow
        if self.hollow:
            self.k = k
        self.material = material
        super(Axle, self).__init__(**kwargs)

    V = Valid()
    r""":class:`.Valid`: Shear load on axle :math:`V` in :math:`[M^{1} L^{1} t^{-2}]`"""

    M = Valid()
    r""":class:`.Valid`: Bending load on axle :math:`M` in :math:`[M^{1} L^{2} t^{-2}]`"""

    T = Valid()
    r""":class:`.Valid`: Torque load on axle :math:`T` in :math:`[M^{1} L^{2} t^{-2}]`"""

    l = Valid()
    r""":class:`.Valid`: Length of the axle :math:`l` in :math:`[L^{1}]`"""

    x = Valid()
    r""":class:`.Valid`: Current position of interrest :math:`x` in :math:`[L^{1}]` """

    d_prime = Valid()
    r""":class:`.Valid`: Rough design estimate of the diameter :math:`d^{\'}` in :math:`[l^{1}]` """

    d = Valid()
    r""":class:`.Valid`: Diameter :math:`d` in :math:`[L^{1}]` """

    axle_type = AxleType.DRIVING

    material = None

    _hollow = False

    @property
    def hollow(self):
        return self._hollow

    @hollow.setter
    def hollow(self, value):
        if value:
            self.d_i = Valid()
            self.d_i = Binder(value=None, unit=u.mm)
            self.k = Valid()
            self.k = Binder(value=None, unit=u.dimensionless)
            self._hollow = True
            # TODO update subscriptions
        else:
            del self.d_i
            del self.k
            self._hollow = False

    def critical_section(self):
        return

