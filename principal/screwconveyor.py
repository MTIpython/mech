from enum import Enum

from MTIpython.units.SI import u
from MTIpython.mtimath.geometry import surface

__all__ = ['Filling', 'working_section', 'conveying_speed', 'volume_flowrate', 'mass_flowrate', 'power_inclination', 'power_noload', 'power_for_progress', 'power_total']


class Filling(Enum):
    r"""
    Filling coefficient, used in Auger calculations source: NEN-ISO 7119:1982
    """

    EASY = 0.45
    r"""
    Materials which flow easily, scarcely abrasive (flour, cereals) source: NEN-ISO 7119:1982 """

    AVERAGE = 0.3
    r"""
    Matter with average abrasive properties, with a grading varying from grains to small lumps (salt, 
    sand, coal) source: NEN-ISO 7119:1982"""

    HEAVY = 0.15
    r"""
    For heavy bulk materials, very abrasive, aggressive (ash, sand, minerals) source: NEN-ISO 7119:1982 """


def working_section(phi: Filling, d):
    r"""
    A

    Args:
        phi:
        d:

    Returns:

    """
    return surface.from_diameter_disc(d) * phi.value


def conveying_speed(S, omega):
    r"""
    v

    Args:
        pitch:
        rotational_speed:

    Returns:

    """
    return S * omega


def volume_flowrate(phi, d, S, omega):
    r"""
    I_v

    Args:
        phi:
        d:
        S:
        omega:

    Returns:

    """
    return working_section(phi, d) * conveying_speed(S, omega)


def mass_flowrate(rho, I_v):
    return rho * I_v


# def power_total(phi, d, S, omega, rho, l, lamda, dh):
#     r"""
#
#
#     Args:
#         phi:
#         d:
#         S:
#         omega:
#         rho:
#         l:
#         lamda:
#         dh:
#
#     Returns:
#
#     """
#     I_v = volume_flowrate(phi, d, S, omega)
#     I_m = mass_flowrate(rho, I_v)
#     P_h = power_for_progress(I_m, l, lamda)
#     P_n = power_noload(d, l)
#     P_st = power_inclination(I_m, dh)
#     return P_h + P_n + P_st

def power_total(phi, d, m_dot, l, lamda, dh):
    r"""


    Args:
        phi:
        d:
        S:
        omega:
        rho:
        l:
        lamda:
        dh:

    Returns:

    """
    I_m = phi.value * m_dot
    P_h = power_for_progress(I_m, l, lamda)
    P_n = power_noload(d, l)
    P_st = power_inclination(I_m, dh)
    return (P_h + P_n + P_st).to('kW')


def power_for_progress(I_m, l, lamda):
    return I_m * l * u.g_n * lamda


def power_noload(d, l):
    return d * l / 20. * u.kW / u.m ** 2


def power_inclination(I_m, dh):
    return I_m * dh * u.g_n
