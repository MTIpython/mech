r"""
.. module:: MTIpython.mech.principal.axle
    :platform: Unix, Windows
    :synopsis: Core axle functions

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from MTIpython.core.functions import MTIfunc, function_switch
from MTIpython.mtimath.types import pi
from MTIpython.reference.bibliography import citation

__all__ = ['diameter', 'diameter_prime']

_source_roloff = r"""@book{roloff_2011,
                    address = {Den Haag},
                    edition = {4de},
                    title = {Roloff/ {Matek} {Machineonderdelen}: {Theoriebook}},
                    isbn = {978-90-395-2321-6},
                    language = {nl},
                    publisher = {SDU Uitgevers},
                    author = {Muhs, Dieter and Wittel, Herbert and Becker, Manfred and Jannasch, Dieter and Vossiek, 
                    Joachim},
                    year = {2011}
}
"""


class _FullDiameter(MTIfunc):
    r"""
    Axle diameter :math:`d` in :math:`[L^{1}]` for full axle.

    **Bending moment**

    .. math::
       d \geq \sqrt[3]{\frac{32 M}{\pi \bar{\sigma}_{b}}}

    **Torque moment**

    .. math::
       d \geq \sqrt[3]{\frac{16 T}{\pi \bar{\tau}_{t}}}

    Args:
        M (:class:`~pint:.Quantity`): Moment :math:`M` on which the axle is subject to in
         :math:`[L^{2} M^{1} t^{-2}]`
        T (:class:`~pint:.Quantity`): Moment :math:`T` on which the axle is subject to in
         :math:`[L^{2} M^{1} t^{-2}]`
        sigma_b (:class:`~pint:.Quantity`): Allowable flexural strength bending :math:`\bar{\sigma}_{b}`
         in :math:`[M^{1} L^{3} t^{-2}]`
        tau_t (:class:`~pint:.Quantity`): Allowable flexural strength for torsion :math:`\bar{\tau}_{t}`
         in :math:`[M^{1} L^{3} t^{-2}]`

    Kwargs combinations:
        * M, sigma_b
        * T, tau_t

    Returns:
        :class:`~pint:.Quantity`: Axle diameter :math:`d` in :math:`[L^{1}]`
    """

    def __init__(self, **kwargs):
        super(_FullDiameter, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_FullDiameter.bending,
                               _FullDiameter.torque,
                               **kwargs)

    @staticmethod
    @citation(_source_roloff)
    def bending(M, sigma_b):
        r"""
        Axle diameter :math:`d` in :math:`[L^{1}]` for full axle subject to a bending moment

        .. math::
           d \geq \sqrt[3]{\frac{32 M}{\pi \bar{\sigma}_{b}}}

        Args:
            M (:class:`~pint:.Quantity`): Moment :math:`M` on which the axle is subject to in
             :math:`[L^{2} M^{1} t^{-2}]`
            sigma_b (:class:`~pint:.Quantity`): Allowable flexural strength bending :math:`\bar{\sigma}_{b}`
             in :math:`[M^{1} L^{3} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Axle diameter :math:`d` in :math:`[L^{1}]`
        """
        return (32. * M / (pi * sigma_b)) ** (1 / 3)

    @staticmethod
    @citation(_source_roloff)
    def torque(T, tau_t):
        r"""
        Axle diameter :math:`d` in :math:`[L^{1}]` for full axle subject to a torque moment

        .. math::
           d \geq \sqrt[3]{\frac{16 T}{\pi \bar{\tau}_{t}}}

        Args:
            T (:class:`~pint:.Quantity`): Moment :math:`T` on which the axle is subject to in
             :math:`[L^{2} M^{1} t^{-2}]`
            tau_t (:class:`~pint:.Quantity`): Allowable flexural strength for torsion :math:`\bar{\tau}_{t}`
             in :math:`[M^{1} L^{3} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Axle diameter :math:`d` in :math:`[L^{1}]`
        """
        return (16. * T / (pi * tau_t)) ** (1 / 3)


class _HollowDiameter(MTIfunc):
    r"""
    Axle diameter :math:`d_{a}` in :math:`[L^{1}]` for a hollow axle

    **Bending moment**

    .. math::
       d_{a} \geq \sqrt[3]{\frac{32 M}{\pi \left(1-k^4\right) \bar{\sigma}_{b}}}

    **Torque moment**

    .. math::
       d_{a} \geq \sqrt[3]{\frac{16 T}{\pi \left(1-k^4\right) \bar{\tau}_{t}}}

    Args:
        M (:class:`~pint:.Quantity`): Moment :math:`M` on which the axle is subject to in
         :math:`[L^{2} M^{1} t^{-2}]`
        T (:class:`~pint:.Quantity`): Moment :math:`T` on which the axle is subject to in
         :math:`[L^{2} M^{1} t^{-2}]`
        sigma_b (:class:`~pint:.Quantity`): Allowable flexural strength bending :math:`\bar{\sigma}_{b}`
         in :math:`[M^{1} L^{3} t^{-2}]`
        tau_t (:class:`~pint:.Quantity`): Allowable flexural strength for torsion :math:`\bar{\tau}_{t}`
         in :math:`[M^{1} L^{3} t^{-2}]`
        k (:class:`~pint:.Quantity`): Diameter ratio :math:`k` in :math:`[-]` defined as :math:`\frac{d_i}{d_a}`

    Kwargs combinations:
        * M, sigma_b, k
        * T, tau_t, k

    Returns:
        :class:`~pint:.Quantity`: Axle diameter :math:`d_{a}` in :math:`[L^{1}]`
    """

    def __init__(self, **kwargs):
        super(_HollowDiameter, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_HollowDiameter.bending,
                               _HollowDiameter.torque,
                               **kwargs)

    @staticmethod
    @citation(_source_roloff)
    def bending(M, sigma_b, k):
        r"""
        Axle diameter :math:`d_{a}` in :math:`[L^{1}]` for a hollow axle subject to a moment

        .. math::
           d_{a} \geq \sqrt[3]{\frac{32 M}{\pi \left(1-k^4\right) \bar{\sigma}_{b}}}

        Args:
            M (:class:`~pint:.Quantity`): Moment :math:`M` on which the axle is subject to in
             :math:`[L^{2} M^{1} t^{-2}]`
            sigma_b (:class:`~pint:.Quantity`): Allowable flexural strength bending :math:`\bar{\sigma}_{b}`
             in :math:`[M^{1} L^{3} t^{-2}]`
            k (:class:`~pint:.Quantity`): Diameter ratio :math:`k` in :math:`[-]` defined as :math:`\frac{d_i}{d_a}`

        Returns:
            :class:`~pint:.Quantity`: Axle diameter :math:`d_{a}` in :math:`[L^{1}]`
        """
        return (32. * M / (pi * (1. - k ** 4) * sigma_b)) ** (1 / 3)

    @staticmethod
    @citation(_source_roloff)
    def torque(T, tau_t, k):
        r"""
        Axle diameter :math:`d_{a}` in :math:`[L^{1}]` for a hollow axle, subject to torque

        .. math::
           d_{a} \geq \sqrt[3]{\frac{16 T}{\pi \left(1-k^4\right) \bar{\tau}_{t}}}

        Args:
            T (:class:`~pint:.Quantity`): Moment :math:`T` on which the axle is subject to in
             :math:`[L^{2} M^{1} t^{-2}]`
            tau_t (:class:`~pint:.Quantity`): Allowable flexural strength for torsion :math:`\bar{\tau}_{t}`
             in :math:`[M^{1} L^{3} t^{-2}]`
            k (:class:`~pint:.Quantity`): Diameter ratio :math:`k` in :math:`[-]` defined as :math:`\frac{d_i}{d_a}`

        Returns:
            :class:`~pint:.Quantity`: Axle diameter :math:`d_{a}` in :math:`[L^{1}]`
        """
        return (16. * T / (pi * (1. - k ** 4) * tau_t)) ** (1 / 3)


class _Diameter(MTIfunc):
    r"""
    Axle diameter :math:`d` in :math:`[L^{1}]`

    **Full axle subject to a bending moment**

    .. math::
       d \geq \sqrt[3]{\frac{32 M}{\pi \bar{\sigma}_{b}}}

    **Full axle subject to a torque moment**

    .. math::
       d \geq \sqrt[3]{\frac{16 T}{\pi \bar{\tau}_{t}}}

    **Hollow axle subject to a bending moment**

    .. math::
       d_{a} \geq \sqrt[3]{\frac{32 M}{\pi \left(1-k^4\right) \bar{\sigma}_{b}}}

    **Hollow axle subject to a torque moment**

    .. math::
       d_{a} \geq \sqrt[3]{\frac{16 T}{\pi \left(1-k^4\right) \bar{\tau}_{t}}}

    **Hollow axle inner diameter**

    .. math::
       d_i \leq k d_a

    Args:
        M (:class:`~pint:.Quantity`): Moment :math:`M` on which the axle is subject to in :math:`[L^{2} M^{1} t^{-2}]`
        T (:class:`~pint:.Quantity`): Moment :math:`T` on which the axle is subject to in :math:`[L^{2} M^{1} t^{-2}]`
        sigma_b (:class:`~pint:.Quantity`): Allowable flexural strength bending :math:`\bar{\sigma}_{b}`
         in :math:`[M^{1} L^{3} t^{-2}]`
        tau_t (:class:`~pint:.Quantity`): Allowable flexural strength for torsion :math:`\bar{\tau}_{t}`
         in :math:`[M^{1} L^{3} t^{-2}]`
        k (:class:`~pint:.Quantity`): Diameter ratio :math:`k` in :math:`[-]` defined as :math:`\frac{d_i}{d_a}`
        d_a (:class:`~pint:.Quantity`): Outside diameter for a hollow axle :math:`d_a` in :math:`[L^{1}]`

    Kwargs combinations:
        * M, sigma_b
        * T, tau_t
        * M, sigma_b, k
        * T, tau_t, k
        * d_a, k

    Returns:
        :class:`~pint:.Quantity`: Axle diameter :math:`d` in :math:`[L^{1}]`
    """

    def __init__(self, **kwargs):
        super(_Diameter, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_FullDiameter.bending,
                               _FullDiameter.torque,
                               _HollowDiameter.bending,
                               _HollowDiameter.torque,
                               _Diameter.inner,
                               **kwargs)

    @staticmethod
    @citation(_source_roloff)
    def inner(d_a, k):
        r"""
        Inner diameter :math:`d_i` in :math:`[l^{1}]`

        .. math::
           d_i \leq k d_a

        Args:
            k (:class:`~pint:.Quantity`): Diameter ratio :math:`k` in :math:`[-]` defined as :math:`\frac{d_i}{d_a}`
            d_a (:class:`~pint:.Quantity`): Outside diameter for a hollow axle :math:`d_a` in :math:`[L^{1}]`

        Returns:
            :class:`~pint:.Quantity`: Inner diameter :math:`d_i` in :math:`[L^{1}]`
        """
        return k * d_a

    full = _FullDiameter()

    hollow = _HollowDiameter()


diameter = _Diameter()


class _FullDiameterPrime(MTIfunc):
    r"""
    Diameter approximation :math:`d^{'}` in :math:`[L^{1}]` for an axle.

    **Load bearing axle**

    .. math::
       d^{'} \approx 3.4 \sqrt[3]{\frac{M}{\sigma_{bD}}}

    eq. 11.16 :cite:`roloff_2011`

    **Driving axle**

    .. math::
       d^{'} \approx 2.7 \sqrt[3]{\frac{T}{\tau_tD}}

    eq. 11.13 :cite:`roloff_2011`

    **Driving axle subject to a moment with a short bearing support distance**

    .. math::
       d^{'} \approx 3.5 \sqrt[3]{\frac{T}{\sigma_{bD}}}

    eq. 11.14 :cite:`roloff_2011`

    **Driving axle subject to a moment with a long bearing support distance**

    .. math::
       d^{'} \approx 4.5 \sqrt[3]{\frac{T}{\sigma_{bD}}}

    eq. 11.15 :cite:`roloff_2011`

    Args:
        T (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
        M (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
        sigma_bD (:class:`~pint:.Quantity`): Bending stress depend on load :math:`\sigma_{bD}` in
         :math:`[M^{1} L^{3} t^{-2}]`
        tau_tD (:class:`~pint:.Quantity`): torque stress depend on load :math:`\tau_{tD}` in
         :math:`[M^{1} L^{3} t^{-2}]`
        short_support (bool): Relative short distance between bearing supports (default) True

    Kwargs combinations:
        * M, sigma_bD
        * T, sigma_bD, short_support
        * T, tau_tD

    Returns:
        :class:`~pint:.Quantity`: Diameter approximation :math:`d^{'}` in :math:`[L^{1}]` for an axle
    """

    def __init__(self, **kwargs):
        super(_FullDiameterPrime, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_FullDiameterPrime.load_bearing,
                               _FullDiameterPrime.driving_with_bending,
                               _FullDiameterPrime.driving,
                               **kwargs)

    @staticmethod
    @citation(_source_roloff)
    def load_bearing(M, sigma_bD):
        r"""
        Diameter approximation :math:`d^{'}` in :math:`[L^{1}]` for a load bearing axle.

        .. math::
           d^{'} \approx 3.4 \sqrt[3]{\frac{M}{\sigma_{bD}}}

        eq. 11.16 :cite:`roloff_2011`

        Args:
            M (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
            sigma_bD (:class:`~pint:.Quantity`): Bending stress depend on load :math:`\sigma_{bD}` in
             :math:`[M^{1} L^{3} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Diameter approximation :math:`d^{'}` in :math:`[L^{1}]` for a load bearing axle
        """
        return 3.4 * (M / sigma_bD) ** (1 / 3)

    @staticmethod
    @citation(_source_roloff)
    def driving_with_bending(T, sigma_bD, short_support=True):
        r"""
        Diameter approximation :math:`d^{'}` in :math:`[L^{1}]` for a driving axle, subject to a bending moment.

        **Short Bearing support distance**

        .. math::
           d^{'} \approx 3.5 \sqrt[3]{\frac{T}{\sigma_{bD}}}

        eq. 11.14 :cite:`roloff_2011`

        **Long Bearing support distance**

        .. math::
           d^{'} \approx 4.5 \sqrt[3]{\frac{T}{\sigma_{bD}}}

        eq. 11.15 :cite:`roloff_2011`

        Args:
            T (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
            sigma_bD (:class:`~pint:.Quantity`): Bending stress depend on load :math:`\sigma_{bD}` in
             :math:`[M^{1} L^{3} t^{-2}]`
            short_support (bool): Relative short distance between bearing supports (default) True

        Returns:
            :class:`~pint:.Quantity`: Diameter approximation :math:`d^{'}` in :math:`[L^{1}]` for a driving axle
        """
        if short_support:
            return 3.5 * (T / sigma_bD) ** (1 / 3)
        else:
            return 4.5 * (T / sigma_bD) ** (1 / 3)

    @staticmethod
    @citation(_source_roloff)
    def driving(T, tau_tD):
        r"""
        Diameter approximation :math:`d^{'}` in :math:`[L^{1}]` for a driving axle.

        .. math::
           d^{'} \approx 2.7 \sqrt[3]{\frac{T}{\tau_{tD}}}

        eq. 11.13 :cite:`roloff_2011`

        Args:
            T (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
            tau_tD (:class:`~pint:.Quantity`): torque stress depend on load :math:`\tau_{tD}` in
             :math:`[M^{1} L^{3} t^{-2}]`

        Returns:
            :class:`~pint:.Quantity`: Diameter approximation :math:`d^{'}` in :math:`[L^{1}]` for a driving axle
        """
        return 2.7 * (T / tau_tD) ** (1 / 3)


class _HollowDiameterPrime(MTIfunc):
    r"""
    Diameter approximation :math:`d^{'}` in :math:`[L^{1}]` for an axle.

    **Load bearing axle**

    .. math::
       d^{'} \approx 3.4 \sqrt[3]{\frac{M}{\sigma_{bD}}}

    eq. 11.16 :cite:`roloff_2011`

    **Driving axle**

    .. math::
       d^{'} \approx 2.7 \sqrt[3]{\frac{T}{\tau_tD}}

    eq. 11.13 :cite:`roloff_2011`

    **Driving axle subject to a moment with a short bearing support distance**

    .. math::
       d^{'} \approx 3.5 \sqrt[3]{\frac{T}{\sigma_{bD}}}

    eq. 11.14 :cite:`roloff_2011`

    **Driving axle subject to a moment with a long bearing support distance**

    .. math::
       d^{'} \approx 4.5 \sqrt[3]{\frac{T}{\sigma_{bD}}}

    eq. 11.15 :cite:`roloff_2011`

    Args:
        T (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
        M (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
        sigma_bD (:class:`~pint:.Quantity`): Bending stress depend on load :math:`\sigma_{bD}` in
         :math:`[M^{1} L^{3} t^{-2}]`
        tau_tD (:class:`~pint:.Quantity`): torque stress depend on load :math:`\tau_{tD}` in
         :math:`[M^{1} L^{3} t^{-2}]`
        short_support (bool): Relative short distance between bearing supports (default) True

    Kwargs combinations:
        * M, sigma_bD
        * T, tau_tD

    Returns:
        :class:`~pint:.Quantity`: Diameter approximation :math:`d^{'}` in :math:`[L^{1}]` for an axle
    """

    def __init__(self, **kwargs):
        super(_HollowDiameterPrime, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_HollowDiameterPrime.load_bearing,
                               _HollowDiameterPrime.driving,
                               **kwargs)

    @staticmethod
    @citation(_source_roloff)
    def load_bearing(M, sigma_bD, k):
        r"""
        Diameter approximation :math:`d_a^{'}` in :math:`[L^{1}]` for a hollow load bearing axle.

        .. math::
           d_a^{'} \approx 3.4 \sqrt[3]{\frac{M}{\left(1-k^4\right)\sigma_{bD}}}

        eq. 11.17 :cite:`roloff_2011`

        Args:
            M (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
            sigma_bD (:class:`~pint:.Quantity`): Bending stress depend on load :math:`\sigma_{bD}` in
             :math:`[M^{1} L^{3} t^{-2}]`
            k (:class:`~pint:.Quantity`): Diameter ratio :math:`k` in :math:`[-]` defined as :math:`\frac{d_i}{d_a}`

        Returns:
            :class:`~pint:.Quantity`: Diameter approximation :math:`d_a^{'}` in :math:`[L^{1}]` for a hollow load
            bearing axle
        """
        return 3.4 * (M / ((1 - k ** 4) * sigma_bD)) ** (1 / 3)

    @staticmethod
    @citation(_source_roloff)
    def driving(T, tau_tD, k):
        r"""
        Diameter approximation :math:`d_a^{'}` in :math:`[L^{1}]` for a hollow driving axle.

        .. math::
           d_a^{'} \approx 2.7 \sqrt[3]{\frac{T}{\left(1-k^4\right) \tau_{tD}}}

        eq. 11.12 :cite:`roloff_2011`

        Args:
            T (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
            tau_tD (:class:`~pint:.Quantity`): torque stress depend on load :math:`\tau_{tD}` in
             :math:`[M^{1} L^{3} t^{-2}]`
            k (:class:`~pint:.Quantity`): Diameter ratio :math:`k` in :math:`[-]` defined as :math:`\frac{d_i}{d_a}`

        Returns:
            :class:`~pint:.Quantity`: Diameter approximation :math:`d_a^{'}` in :math:`[L^{1}]` for a hollow driving
            axle
        """
        return 2.7 * (T / ((1 - k ** 4) * tau_tD)) ** (1 / 3)


class _DiameterPrime(MTIfunc):
    r"""
    Diameter approximation :math:`d^{'}` or :math:`d_a^{'}` in :math:`[L^{1}]` for an (hollow) axle.

    **Load bearing axle**

    .. math::
       d^{'} \approx 3.4 \sqrt[3]{\frac{M}{\sigma_{bD}}}

    eq. 11.16 :cite:`roloff_2011`

    **Load bearing hollow axle**

    .. math::
       d_a^{'} \approx 3.4 \sqrt[3]{\frac{M}{\left(1-k^4\right)\sigma_{bD}}}

    eq. 11.17 :cite:`roloff_2011`

    **Driving axle**

    .. math::
       d^{'} \approx 2.7 \sqrt[3]{\frac{T}{\tau_tD}}

    eq. 11.13 :cite:`roloff_2011`

    **Hollow driving axle**

    .. math::
       d_a^{'} \approx 2.7 \sqrt[3]{\frac{T}{\left(1-k^4\right) \tau_{tD}}}

    eq. 11.12 :cite:`roloff_2011`

    **Driving axle subject to a moment with a short bearing support distance**

    .. math::
       d^{'} \approx 3.5 \sqrt[3]{\frac{T}{\sigma_{bD}}}

    eq. 11.14 :cite:`roloff_2011`

    **Driving axle subject to a moment with a long bearing support distance**

    .. math::
       d^{'} \approx 4.5 \sqrt[3]{\frac{T}{\sigma_{bD}}}

    eq. 11.15 :cite:`roloff_2011`

    Args:
        T (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
        M (:class:`~pint:.Quantity`): Bending moment :math:`M_b` in :math:`[M^{1} L^{2} t^{-2}]`
        sigma_bD (:class:`~pint:.Quantity`): Bending stress depend on load :math:`\sigma_{bD}` in
         :math:`[M^{1} L^{3} t^{-2}]`
        tau_tD (:class:`~pint:.Quantity`): torque stress depend on load :math:`\tau_{tD}` in
         :math:`[M^{1} L^{3} t^{-2}]`
        short_support (bool): Relative short distance between bearing supports (default) True
        k (:class:`~pint:.Quantity`): Diameter ratio :math:`k` in :math:`[-]` defined as :math:`\frac{d_i}{d_a}`

    Kwargs combinations:
        * M, sigma_bD
        * M, sigma_bD, k
        * T, sigma_bD, short_support
        * T, tau_tD
        * T, tau_tD, k

    Returns:
        :class:`~pint:.Quantity`: Diameter approximation :math:`d^{'}` or :math:`d_a^{'}` in :math:`[L^{1}]` for an axle
    """

    def __init__(self, **kwargs):
        super(_DiameterPrime, self).__init__(**kwargs)

    def __call__(self, **kwargs):
        return function_switch(_FullDiameterPrime.driving_with_bending,
                               _FullDiameterPrime.driving,
                               _FullDiameterPrime.load_bearing,
                               _HollowDiameterPrime.load_bearing,
                               _HollowDiameterPrime.driving,
                               **kwargs)

    full = _FullDiameterPrime()

    hollow = _HollowDiameterPrime()


diameter_prime = _DiameterPrime()
