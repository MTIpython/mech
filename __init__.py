r"""
.. module:: MTIpython.mech
    :platform: Unix, Windows
    :synopsis: Mechanical engineering module

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from MTIpython.mech import auger, principal

__all__ = ['principal', 'auger', 'axle']
